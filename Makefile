PREFIX=/usr/local/

build :
	$(CC) after.c -o after

uninstall :
	rm $(PREFIX)/bin/after
	rm $(PREFIX)/man/man1/after.1

install : build
	install after $(PREFIX)bin/after
	install after.1 $(PREFIX)/man/man1/after.1


