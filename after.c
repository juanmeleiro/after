#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>

#define ARGSPEC ":d:h"
#define USAGE "usage: after [-d DELTA] pid"
#define DETAILS "options:\n    -d DELTA number of seconds to sleep\n    -h Show this help message"

void
die(int err, const char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);

  if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
    fputc(' ', stderr);
    perror(NULL);
  } else {
    fputc('\n', stderr);
  }

  exit(err);
}

int
isdigits(char* s) {
  char *p;
  for (p = s; isdigit(*p); p++);
  return (*p == '\0');
}

int
main(int argc, char **argv) {
  int pid, delta = 1, done;
  char c;

  if (argc < 2) die(1, USAGE);

  while ((c = getopt(argc, argv, ARGSPEC)) != -1) {
    switch (c) {
    case 'h':
      puts(USAGE);
      puts(DETAILS);
      exit(0);
      break;
    case 'd':
      if (!isdigits(optarg)) die(EINVAL, "Could not parse %s as an integer.", optarg);
      delta = atoi(optarg);
      if (delta == 0) die(EINVAL, "Zero is not an allowed value for -d.");
      getopt(argc, argv, ARGSPEC); /* Get to the first non-option argument */
      break;
    case '?':
      die(1, "Unknown option -%c.", optopt);
    case ':':
      die(1, "Option -%c requires an argument.", optopt);
    }
  }

  if (argv[optind] == NULL) die(EINVAL, "Argument PID is required.");

  if (!isdigits(argv[optind])) die(EINVAL, "Could not parse %s as an integer.", argv[optind]);
  pid = atoi(argv[optind]);

  while (getpgid(pid) >= 0) {
    sleep(delta);
  }

  return 0;
}
