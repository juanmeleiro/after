# after

`after(1)` takes a PID and just waits for it to no longer exist, and then exits.

Do you have to leave and have to shutdown your computer after it finishes performing a backup? You already started the backup program and don't want to stop it?

```
$ after (pgrep backup); shutdown now
```

## Installation

```
$ make
$ sudo make install
```

